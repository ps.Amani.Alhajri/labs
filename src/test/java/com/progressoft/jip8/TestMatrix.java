package com.progressoft.jip8;

public class TestMatrix {

    public static void main(String[] args) {
        int[][] m1 = new int[][]{
                {1,2,3},
                {3,4,5},
                {5,6,7}
        };
        // array is not a matrix:
        // inconsistent rows length
        int[][] m2 = new int[][]{
                {3,2,1},
                null,
                {6,8,7}
        };

        MatrixUtility utility = new MatrixUtility();
        int[][] result = utility.sum(m1, m2);
        utility.print(result);
    }
}
