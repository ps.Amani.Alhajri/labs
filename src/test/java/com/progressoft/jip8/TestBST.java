package com.progressoft.jip8;

import org.junit.Test;

public class TestBST {

    @Test
    public void testFixedQueue() {

        BinaryTree bt = new BinaryTree(10);
        bt.accept(6);
        bt.accept(18);
        bt.accept(4);
        bt.accept(8);
        bt.accept(15);
        bt.accept(21);
        //t.accept(9);
        bt.traverseInOrder(bt.root);
        System.out.println();
        System.out.println(bt.depth(bt.root, 10));
        System.out.println(bt.treeDepth(bt.root));
    }
}
