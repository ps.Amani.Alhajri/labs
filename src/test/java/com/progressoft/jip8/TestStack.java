package com.progressoft.jip8;

import org.junit.Test;

public class TestStack {
    @Test

    public void testFixedStack() {
        FixedStack stack = new FixedStack(8);
        //System.out.println(s.size());
        //System.out.println(s.peek());
        stack.push("1");
        stack.push("2");
        stack.push("3");
        System.out.println(stack.size());
        stack.push("4");
        stack.push("5");
        stack.push("6");
        stack.push("7");
        System.out.println(stack.size());
        stack.Print();
        System.out.println(stack.peek());
/*
        s.pop();
        s.pop();
        s.pop();
        s.pop();
        System.out.println(s.size());
        s.pop();
        s.pop();
        s.pop();
       // System.out.println(s.peek());
       if(s.isEmpty())
            System.out.println("Yes");
        else
            System.out.println("No");
        //System.out.println(s.size());
        //s.push("3");
        //System.out.println(s.peek());
        /*
        s.pop();
        s.pop();
        s.pop();
        s.pop();
        System.out.println(s.size());
        if(s.isEmpty())
            System.out.println("Yes");
        else
            System.out.println("No");*/
    }


    @Test
    public void testFixedQueue() {
        FixedQueue queu= new FixedQueue(4);
        System.out.println(queu.size());
        if(queu.isEmpty())
            System.out.println("Yes");
        else
            System.out.println("No");
        queu.enqueue("55");
        queu.enqueue("2");
        queu.enqueue("3");
        queu.enqueue("4");
        System.out.println(queu.size());
        queu.dequeue();
        queu.dequeue();
        System.out.println(queu.size());
        queu.enqueue("8");
        queu.enqueue("9");
        System.out.println(queu.peek());
        queu.Print();
    }

    @Test
    public void testDynamicQueue() {
        DynamicQueue queue= new DynamicQueue();
        System.out.println(queue.size());
        if(queue.isEmpty())
            System.out.println("Yes");
        else
            System.out.println("No");
        queue.enqueue("1");
        queue.enqueue("2");
        queue.enqueue("3");
        queue.enqueue("4");
        System.out.println("size =  " +queue.size());
        System.out.println("peek =  " + queue.peek());
        queue.Print();
       queue.dequeue();
        queue.dequeue();
        queue.dequeue();

        System.out.println("size =  " +queue.size());
        queue.enqueue("8");
        queue.enqueue("9");
        System.out.println("peek =  " + queue.peek());
        queue.Print();
        if(queue.isEmpty())
            System.out.println("Yes");
        else
            System.out.println("No");
    }
}
