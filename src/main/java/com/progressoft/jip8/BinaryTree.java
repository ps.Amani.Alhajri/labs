package com.progressoft.jip8;

public class BinaryTree {

    public class Node {

        int data;
        Node left;
        Node right;

        Node(int data) {
            this.data = data;
            right = null;
            left = null;
        }

    }

    Node root;


    BinaryTree(int value) {
        root = new Node(value);
    }


    public void accept(int value) {
         insertElement(root, value);
    }

    private boolean insertElement(Node root, int value){
        if (value == root.data)
            return false;
        if( value > root.data ){
            if(root.right == null) {
                root.right = new Node(value);
                return true;
            }
            else
                insertElement(root.right , value);
        }
        else {
            if (root.left == null) {
                root.left = new Node(value);
                return true;
            }
            else
                insertElement(root.left , value);
        }
        return false;
    }



    public int depth(Node node, int value) {
        if (root == null)
            return 0;
        if (root.data == value)
            return 1;
        if(root.data > value)
            return 1 + depth(node.left , value);
        else
            return 1 + depth(node.right , value);
    }



    public int treeDepth(Node node) {

        if (node == null)
            return 0;
        else if (node.left == null && node.right == null)
            return 1;
        else {
            int l = treeDepth(node.left);
            int r = treeDepth(node.right);
            if(l > r)
                return l + 1;
            else
                return r+1;
        }
    }

    public void traverseInOrder(Node node) {
        if (node != null) {
            traverseInOrder(node.left);
            System.out.print(node.data + " ");
            traverseInOrder(node.right);
        }
    }
}
