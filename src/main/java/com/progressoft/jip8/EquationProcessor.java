package com.progressoft.jip8;

import java.util.Stack;

public class EquationProcessor {

        public static Double computeExpression(String expression) {

            Stack<Double> operands = new Stack<Double>();
            Stack<Character> operations = new Stack<Character>();
            char[] charArray = expression.toCharArray();

            for (int i = 0; i < charArray.length; i++) {

                if (charArray[i] == ' ')
                    continue;

                if ((charArray[i] >= '0' && charArray[i] <= '9') || charArray[i] == '.'){
                    String number = "";
                    while (i < charArray.length && ((charArray[i] >= '0' && charArray[i] <= '9') || charArray[i] == '.') )
                        number+=charArray[i++];
                    operands.push(Double.parseDouble(number));
                }

                else if (charArray[i] == '*' || charArray[i] == '/' || charArray[i] == '+' || charArray[i] == '-') {
                    // check *+
                    if (i+1 < charArray.length && (charArray[i+1] == '*' || charArray[i+1] == '/' || charArray[i+1] == '+' || charArray[i+1] == '-') )
                        throw new IllegalArgumentException( expression + " is invalid expression!!");

                    while (!operations.empty() && hasHigherPriority(charArray[i], operations.peek()))
                        operands.push(computeEx(operations.pop(), operands.pop(), operands.pop()));

                    operations.push(charArray[i]);
                }


                else if (charArray[i] == '(')
                    operations.push(charArray[i]);

                else if (charArray[i] == ')') {
                    while (operations.peek() != '(')
                        operands.push(computeEx(operations.pop(), operands.pop(), operands.pop()));
                    operations.pop();
                }

                else
                    throw new IllegalArgumentException(charArray[i] + " is invalid operations!!");


            }

            while (!operations.empty() && !operands.empty() ) {
                Double num1, num2;
                char opr = operations.pop();
                num1 = operands.pop();
                if(!operands.empty()) {
                    num2 = operands.pop();
                    operands.push(computeEx(opr, num1, num2));
                }
                else
                    throw new IllegalArgumentException("Empty Stack!!");
            }

            if (!operands.empty())
                return operands.pop();
            else
                throw new IllegalArgumentException("No Expression to evaluate!!");
        }

    public static Double computeEx(char opr, Double num1, Double num2) throws IllegalArgumentException  {
        switch (opr) {
            case '+':
                return num2 + num1;
            case '-':
                return num2 - num1;
            case '*':
                return num2 * num1;
            case '/':
                if (num1 == 0)
                    throw new ArithmeticException("Division by zero Exception");
                return num2 / num1;
        }
        throw new IllegalArgumentException("Invalid input!!");
    }


    public static boolean hasHigherPriority(char opr1, char opr2) {
            if ((opr2 == '(' || opr2 == ')') || ((opr1 == '*' || opr1 == '/') && (opr2 == '+' || opr2 == '-')) )
                return false;
            else
                return true;
        }


        public static void main(String[] args) {
            try {
                System.out.println(computeExpression(" "));
            }catch (Exception e) {
                System.out.println(e.getMessage());
            }

            try {
                System.out.println(computeExpression("1 +"));
            }catch (Exception e) {
                System.out.println(e.getMessage());
            }

            try {
                System.out.println(computeExpression("1 +* 2"));
            }catch (Exception e) {
                System.out.println(e.getMessage());
            }

            try {
                System.out.println(computeExpression("1 & 2"));
            }catch (Exception e) {
                System.out.println(e.getMessage());
            }

            try {
                System.out.println(computeExpression("1 / 0"));
            }catch (Exception e) {
                System.out.println(e.getMessage());
            }

            try {
                System.out.println(computeExpression("100 * ( 2 + 12 )"));
                System.out.println(computeExpression("31 + 5 * 6 - 7"));
            }catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }