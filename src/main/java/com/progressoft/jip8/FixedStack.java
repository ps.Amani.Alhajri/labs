package com.progressoft.jip8;

public class FixedStack extends Stack{

    private int totalSize;

    FixedStack(int size) {
        arrStack = new String[size];
        totalSize = size;
        topIndex = -1;
    }

    public void push(String str) throws IllegalArgumentException {
        throwIfFull();
        arrStack[++topIndex] = str;
    }

    // TODO return the popped value
    public String pop() throws IllegalArgumentException {
        throwIfEmpty();
        return arrStack[topIndex--];
    }

    public Boolean isFull() {
        return topIndex == totalSize - 1;
    }

    private void throwIfFull() {
        if (isFull())
            throw new IllegalArgumentException("Ops, The Stack is full!!");
    }

}
