package com.progressoft.jip8;

public class FixedQueue extends Queue{


    private int totalSize;


    FixedQueue(int size) {
        arrQueue = new String[size];
        totalSize = size;
        currentSize = -1;
    }


    public void enqueue(String str) throws IllegalArgumentException {
        if (isFull())
            throw new IllegalArgumentException("Ops, The Queue is full!!");

        arrQueue[++currentSize] = str;
    }


    public String dequeue() throws IllegalArgumentException {

        if (isEmpty())
            throw new IllegalArgumentException("Ops, The Queue is empty!!");

        String Result = arrQueue[0];
        if (currentSize >= 0)
            System.arraycopy(arrQueue, 1, arrQueue, 0, currentSize);

        currentSize--;
        return Result;
    }


    public Boolean isFull() { return currentSize == totalSize - 1; }



    public void Print() {
        for (int i = 0; i <= currentSize; i++)
            System.out.print(arrQueue[i] + ", ");
    }

}
