package com.progressoft.jip8;

public class Queue {
    public String[] arrQueue;
    public int currentSize;

    public int size() {
        return currentSize +1;
    }


    public String peek() throws IllegalArgumentException {
        if (isEmpty())
            throw new IllegalArgumentException("Oop, the Queue is empty!!");

        return arrQueue[0];
    }

    public Boolean isEmpty() {
        return currentSize == -1;
    }


}
