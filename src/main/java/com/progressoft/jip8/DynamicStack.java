package com.progressoft.jip8;

import java.util.Arrays;

// TODO there is duplicate code with the FixedStack
public class DynamicStack extends Stack{


    DynamicStack() {
        arrStack = new String[1];
        topIndex = -1;
    }

    public void push(String str) throws ArrayIndexOutOfBoundsException {
        increaseSize();
        arrStack[topIndex] = str;
    }


    public String pop() throws IllegalArgumentException {
        throwIfEmpty();
        String result =  arrStack[topIndex];
        decreaseSize();
        return result;
    }

    private void decreaseSize() {
        topIndex--;

        String[] temp = Arrays.copyOf(arrStack, arrStack.length);
        if (topIndex >= 0) {
            arrStack = new String[topIndex];
            System.arraycopy(temp, 1, arrStack, 0, topIndex);
        }
    }
    private void increaseSize() {
        topIndex++;

        String[] temp = new String[arrStack.length];
        System.arraycopy(arrStack, 0, temp, 0, arrStack.length);
        arrStack = new String[arrStack.length + 1];
        System.arraycopy(temp, 0, arrStack, 0, arrStack.length - 1);

    }

}
