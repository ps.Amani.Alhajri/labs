package com.progressoft.jip8;

import java.util.Arrays;

public class DynamicQueue extends Queue {


    DynamicQueue() {
        arrQueue = new String[0];
        currentSize = -1;
    }


    public void enqueue(String str) {
       increaseSize();
        arrQueue[currentSize] = str;
    }

    public void increaseSize() {
        currentSize++;
        String[] temp  = new String[arrQueue.length];
        System.arraycopy(arrQueue, 0, temp, 0, arrQueue.length); // Copy the elements into temp
        arrQueue = new String[arrQueue.length + 1];
        System.arraycopy(temp, 0, arrQueue, 0, arrQueue.length - 1);
    }



    public String dequeue() throws IllegalArgumentException {
        if (isEmpty())
            throw new IllegalArgumentException("Ops, The Queue is empty!!");

        String Result = arrQueue[0];
        decreaseSize();
        return Result;
    }

    public void decreaseSize() {
        currentSize--;
        if(currentSize >= 0) {
            String[] temp  = new String[arrQueue.length];
            System.arraycopy(arrQueue, 0, temp, 0, arrQueue.length);
            arrQueue = new String[arrQueue.length-1];
            System.arraycopy(temp, 1, arrQueue, 0, arrQueue.length);
        }
    }


    public void Print() {
        for (int i = 0; i < arrQueue.length; i++)
            System.out.print(arrQueue[i] + ", ");
    }

}
