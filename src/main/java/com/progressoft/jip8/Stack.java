package com.progressoft.jip8;

public class Stack {

    public String[] arrStack;
    public int topIndex;


    public Boolean isEmpty() {
        return topIndex == -1;
    }

    public int size() {
        return topIndex + 1;
    }

    public String peek() throws IllegalArgumentException {
        throwIfEmpty();
        return returnTopValue();
    }
    public void Print() {
        for (int i = topIndex; i >= 0; i--)
            System.out.print(arrStack[i] + ", ");
    }

    public void throwIfEmpty() {
        if (isEmpty())
            throw new IllegalArgumentException("Ops, The Stack is empty!!");
    }
    private String returnTopValue() {
        return arrStack[topIndex];
    }


}
