package com.progressoft.jip8;


import java.util.*;

public class MatrixUtility {


    public int[][] sum(int[][] first, int[][] second) throws IllegalArgumentException {


        if (first.length != second.length) {
            throw new IllegalArgumentException("Ops, The rows of two arrays are not the same!!"); // Terminate and throw an error message
        }
        if (first[0].length != second[0].length) {
            throw new IllegalArgumentException("Ops, The columns of two arrays are not the same!!"); // Terminate and throw an error message
        }

        isValidMatrix(first);
        isValidMatrix(second);


        int[][] result = new int[first.length][first[0].length];

        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < first[i].length; j++) {
                result[i][j] = first[i][j] + second[i][j];
            }
        }

        return result;

    }

    public int[][] scale(int[][] matrix, int scaler) {
        isValidMatrix(matrix);
        int[][] result = new int[matrix.length][matrix[0].length];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result[i][j] = scaler * matrix[i][j];
            }
        }

        return result;
    }

    public int[][] transpose(int[][] matrix) {
        isValidMatrix(matrix);
        int[][] result = new int[matrix.length][matrix[0].length];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result[j][i] = matrix[i][j];
            }
        }
        return result;
    }

    public int[][] multiply(int[][] first, int[][] second) throws IllegalArgumentException {
        isValidMatrix(first);
        isValidMatrix(second);
        if (first[0].length != second.length) {
            throw new IllegalArgumentException("Ops, The number of columns of the left matrix is not the same as the number of rows of the right matrix!!");
        }

        int[][] result = new int[first.length][second[0].length];

        for (int row = 0; row < first.length; row++) {
            for (int col = 0; col < second[0].length; col++) {
                int sum = 0;
                for (int t = 0; t < first[0].length; t++) {
                    sum += first[row][t] * second[t][col];
                }
                result[row][col] = sum;
            }
        }

        return result;
    }

    public int[][] subMatrix(int[][] matrix, int rowToRemove, int colToRemove) throws IllegalArgumentException {
        isValidMatrix(matrix);
        if (rowToRemove > matrix.length || colToRemove > matrix[0].length)
            throw new IllegalArgumentException("Ops, The n-by-m is greater than the matrix!!");

        if (rowToRemove <= 0 || colToRemove <= 0)
            throw new IllegalArgumentException("Ops, rowToRemove or colToRemove are in minus!!");

        int[][] result = new int[matrix.length][matrix[0].length];
        int fillingRow = 0;
        for (int i = 0; i < matrix.length; i++) {
            if (i == rowToRemove - 1) {
                continue;
            }
            int fillingCol = 0;
            for (int j = 0; j < matrix[i].length; j++) {
                if (j == colToRemove - 1) {
                    continue;
                }
                result[fillingRow][fillingCol++] = matrix[i][j];

            }
            fillingRow++;
        }

        return result;
    }

    // Square Matrix operations: Those operations are made to a square matrix where number of columns is equal to the
    // number of rows. copyType represent the user choice if it is
    // D for Change Matrix to a diagonal matrix
    // L for Change matrix to a lower triangular matrix
    // U for Change matrix to a upper triangular matrix
    public int[][] copySquareMatrix(int[][] matrix, char copyType) throws IllegalArgumentException {

        isValidMatrix(matrix);
        if (matrix.length != matrix[0].length)
            throw new IllegalArgumentException("Ops, It's not a square Matrix !!");


        copyType = Character.toUpperCase(copyType);

        if (copyType == 'D' || copyType == 'L' || copyType == 'U')
            return doDiagonal(matrix, copyType);

        throw new IllegalArgumentException("Invalid input,\n Enter D for Change Matrix to a diagonal matrix \n L for Change matrix to a lower triangular matrix \n U for Change matrix to a upper triangular matrix!!");

    }

    private int[][] doDiagonal(int[][] matrix, char copyType) {
        isValidMatrix(matrix);
        CopyCondition condition = getCopyCondition(copyType);
        return copyMatrix(matrix, condition);
    }

    private int[][] copyMatrix(int[][] matrix, CopyCondition condition) {
        int[][] result = new int[matrix.length][matrix[0].length];
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                if (condition.doCopy(row, col))
                    result[row][col] = matrix[row][col];
                else
                    result[row][col] = 0;
            }
        }
        return result;
    }

    private CopyCondition getCopyCondition(char copyType) {
        switch (copyType) {
            case 'D':
                return this::isDiagonal;
            case 'U':
                return this::isUpper;
            case 'L':
                return this::isLower;
            default:
                throw new IllegalArgumentException("unknown copy type: " + copyType);
        }
    }

    private interface CopyCondition {

        boolean doCopy(int row, int col);
    }

    private boolean isUpper(int row, int col) {
        return row <= col;
    }

    private boolean isLower(int row, int col) {
        return row >= col;
    }

    private boolean isDiagonal(int row, int col) {
        return row == col;
    }

    // TODO you should verify passed array as a valid matrix
    private void isValidMatrix(int matrix[][]) {
        isNull(matrix);
        inconsistentArray(matrix);
    }

    private void isNull(int[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            if (matrix[row] == null)
                throw new NullPointerException("row " + row + " is null");
        }
    }

    private void inconsistentArray(int[][] matrix) {
        for (int row = 1; row < matrix.length; row++) {
            if (matrix[row].length != matrix[0].length)
                throw new IllegalArgumentException("inconsistent matrix");
        }
    }

    public int determinant(int[][] matrix) throws IllegalArgumentException {
        isValidMatrix(matrix);
        if (matrix.length != matrix[0].length) {
            throw new IllegalArgumentException("Ops, It's not a square Matrix !!");
        }

        return determinant(matrix, matrix.length); // get a determinant of a (N x N) matrix

    }

    private int determinant(int matrix[][], int dimension) {

        if (dimension == 1) {
            return matrix[0][0];
        }

        int determinant = 0;

        int signFlag = 1;
        for (int i = 0; i < dimension; i++) {
            int[][] sub = subMatrix(matrix, 1, i + 1);
            determinant += signFlag * matrix[0][i] * determinant(sub, dimension - 1);
            signFlag = -signFlag;
        }

        return determinant;
    }

    public void print(int[][] matrix) {
        isValidMatrix(matrix);
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + ", ");
            }
            System.out.println();
        }
    }

}
